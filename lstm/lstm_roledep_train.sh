#!/bin/bash

source ~/.bashrc

traindata=dstc5_train
devdata=dstc5_dev
testdata=dstc5_test
traindata_out=dstc5_train.dat
devdata_out=dstc5_dev.dat
testdata_out=dstc5_test.dat

dataroot=../../../data
ontology=./config/ontology_dstc5.json
wordvector=../../../word2vec/wordembedding.txt

# generate the data
generate_data.py --data $traindata --lan "english" --output $traindata_out --dataroot $dataroot --ontology $ontology
generate_data.py --data $devdata --lan "chinese" --output $devdata_out --dataroot $dataroot --ontology $ontology
#generate_data.py --data $testdata --lan "chinese" --output $testdata_out --dataroot $dataroot --ontology $ontology


n_units=50
learn_rate=0.5
model=model_dstc/lstm${n_units}-R${learn_rate}-role_slot.txt
log=log_dstc/lstm${n_units}-R${learn_rate}-role_slot.log

devdata_prediction=dstc5_dev_slot_pred.dat
# predict the slot value
lstm_roledep_train.py -g0 --train $traindata_out --valid $devdata_out  \
     -m $model -l $log --dataroot $dataroot --ontology $ontology --wordvector $wordvector -u $n_units -R $learn_rate \
     --output $devdata_prediction 

# predict the slot value and generate the tracker
dataname=dstc5_dev
inputfile=$devdata_prediction
predict_slotvalue.py --inputfile $inputfile --dataname $dataname --dataroot $dataroot --ontology $ontology
