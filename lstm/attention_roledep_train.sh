#!/bin/bash

tag_type=base   # Dialog act only
#tag_type=rough # Dialog act + slot type

train=./data/if_jpn_${tag_type}_train.dat
dev=./data/if_jpn_${tag_type}_dev.dat
test=./data/if_jpn_${tag_type}_test.dat

n_units=100
n_attns=100

mkdir -p model log
model=model/attention${n_units}-${n_attns}-${tag_type}-role.txt
log=log/attention${n_units}-${n_attns}-${tag_type}-role.txt

attention_roledep_train.py -g0 --train $train --valid $dev --test $test \
          --num-attns $n_attns -m $model -l $log
