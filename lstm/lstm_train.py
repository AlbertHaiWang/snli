#!/usr/bin/env python
"""Utterance classification using context-sensitive LSTM
   for chainer 1.4.0
"""
import argparse
import math
import sys
import time
import random

import numpy as np
import six

import chainer
from chainer import cuda
import chainer.functions as F
from chainer import optimizers

import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--gpu', '-g', default=1, type=int,
                    help='GPU ID (negative value indicates CPU)')
parser.add_argument('--train', default='', type=str,
                    help='Filename of train data')
parser.add_argument('--valid', default='', type=str,
                    help='Filename of validation data')
parser.add_argument('--test', default='', type=str,
                    help='Filename of test data')
parser.add_argument('--initial-model', '-i', default='', type=str,
                    help='RNN model to be used')
parser.add_argument('--model', '-m', default='model/lstm.mdl', type=str,
                    help='RNN model file to be output')
parser.add_argument('--num-epochs', '-e', default=40, type=int,
                    help='Number of epochs')
parser.add_argument('--num-units', '-u', default=100, type=int,
                    help='Number of hidden units')
parser.add_argument('--num-projs', '-p', default=100, type=int,
                    help='Number of projection layer units')
parser.add_argument('--learn-rate', '-R', default=0.5, type=float,
                    help='Initial learning rate')
parser.add_argument('--learn-decay', '-D', default=0.5, type=float,
                    help='Decaying ratio of learning rate')
parser.add_argument('--log', '-l', default='', type=str,
                    help='Log filename')
args = parser.parse_args()
xp = cuda.cupy if args.gpu >= 0 else np

# open log file
if args.log != '':
    flog = open(args.log, 'w')

random.seed(1)
np.random.seed(1)

n_epochs = args.num_epochs  # number of epochs
n_units = args.num_units  # number of units per layer
n_projs = args.num_projs  # number of units per layer
grad_clip = 5    # gradient norm threshold to clip

print 'Model file:', args.model

# Load text data
def load_data(filename, train=True):
    global ilabelset
    global olabelset
    dataset = []

    print 'Loading', filename

    for s in open(filename).readlines():
        s = s.strip()
        data = s.split(":")
        olabel = data[0]
        ilabels = data[1:]
        ilabels = ''.join(ilabels)
        ilabels = ilabels.split()
        seqdata = np.ndarray((len(ilabels),), dtype=np.int32)
        for i, ilabel in enumerate(ilabels):
            if ilabel in ilabelset:
               seqdata[i] = ilabelset[ilabel]
            else:
               if train==True:
                  seqdata[i] = len(ilabelset)
                  ilabelset[ilabel] = len(ilabelset)
               else:
                  seqdata[i] = unk
            if olabel in olabelset:
                olabel_id = olabelset[olabel]
            else:
                if train==True:
                    olabel_id = len(olabelset)
                    olabelset[olabel] = len(olabelset)
                else:
                    olabel_id = unk
        dataset.append((seqdata, olabel_id))

    return dataset

# Prepare RNN model and load data
if args.initial_model != '':
    print 'Lording model params from', args.initial_model
    fm = open(args.initial_model, 'r')
    ilabelset = pickle.load(fm)
    olabelset = pickle.load(fm)
    model = pickle.load(fm)
    temp,n_units = model.parameters[1].shape
    fm.close()
    train_data = load_data(args.train, train=True)
    valid_data = load_data(args.valid, train=False)
    test_data = load_data(args.test, train=False)
else:
    ilabelset = {}
    olabelset = {}
    eos = 0
    unk = 1
    train_data = load_data(args.train, train=True)
    valid_data = load_data(args.valid, train=False)
    test_data = load_data(args.test, train=False)

    print olabelset

    model = chainer.FunctionSet(embed=F.EmbedID(len(ilabelset), n_projs),
                                l1_x=F.Linear(n_projs, 4 * n_units),
                                l1_h=F.Linear(n_units, 4 * n_units),
                                l2=F.Linear(n_units, len(olabelset)))
    for param in model.parameters:
        param[:] = np.random.uniform(-0.1, 0.1, param.shape)

print '#ilabel =', len(ilabelset)
print '#olabel =', len(olabelset)
print 'train:', len(train_data), 'instances'
print 'valid:', len(valid_data), 'instances'
print 'test:', len(test_data), 'instances'

if args.gpu >= 0:
    cuda.check_cuda_available()
    cuda.get_device(args.gpu).use()
    xp.random.seed(1)
    model.to_gpu()

# Neural net architecture
def forward_one_step(x_data, y_data, state, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    t = chainer.Variable(xp.asarray(y_data), volatile=not train)
    h0 = model.embed(x)
    h1_in = model.l1_x(F.dropout(h0, train=train)) + model.l1_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    y = model.l2(F.dropout(h1, train=train))
    state = {'c1': c1, 'h1': h1}
    if train==True:
        return state, F.softmax_cross_entropy(y, t)
    else:
        return state, F.softmax(y)

def forward_one_step_no_output(x_data, state, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    h0 = model.embed(x)
    h1_in = model.l1_x(F.dropout(h0, train=train)) + model.l1_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    state = {'c1': c1, 'h1': h1}
    return state

def make_initial_state(batchsize=1, train=True):
    return {name: chainer.Variable(xp.zeros((batchsize, n_units),
                                             dtype=np.float32),
                                   volatile=not train)
            for name in ('c1', 'h1')}

# Evaluation routine
def evaluate(eval_data):
    hit = 0
    count = 0
    for j in six.moves.range(len(eval_data)):
        state = make_initial_state(1, train=False)
        iseq = eval_data[j][0]
        olab[0] = eval_data[j][1]
        for ivalue in iseq:
            ilab[0] = ivalue
            state = forward_one_step_no_output(ilab, state, train=False)
        ilab[0] = eos
        state, prob = forward_one_step(ilab, olab, state, train=False)
        pp = cuda.to_cpu(prob.data)
        result = np.argmax(pp[0])
        if result == olab[0]:
           hit += 1
        count += 1

    acc = (float(hit)/count) * 100.0
    return hit, count, acc

############################################
print '----------------'
print 'Start training'
print '----------------'
sys.stdout.flush()
if args.log != '':
    flog.write('Start training\n')
    flog.flush()

# Setup optimizer
optimizer = optimizers.SGD(lr=args.learn_rate)
optimizer.setup(model)

#random.shuffle(train_batchset)
cur_log_perp = xp.zeros(())
epoch = 0
start_at = time.time()
cur_at = start_at
accum_loss = chainer.Variable(xp.zeros((), dtype=np.float32))

model_saved = False
ilab = np.zeros((1,), dtype=np.int32)
olab = np.zeros((1,), dtype=np.int32)

# initial evaluation
valid_hit, valid_count, valid_acc = evaluate(valid_data)
print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
if args.log != '':
    flog.write('  initial valid accuracy = %g (%d/%d)\n' % (valid_acc,valid_hit,valid_count))
    flog.flush()
max_valid_acc = valid_acc

if args.test != '':
    test_hit, test_count, test_acc = evaluate(test_data)
    print ' test accuracy: %g (%d/%d)' % (test_acc, test_hit, test_count)
    if args.log != '':
        flog.write('  initial test accuracy = %g (%d/%d)\n' % (test_acc,test_hit,test_count))
        flog.flush()

n = 0
for i in six.moves.range(n_epochs):
    print '----------------'
    print 'Epoch %d : learning rate = %g' % (i+1, optimizer.lr)
    if args.log != '':
        flog.write('Epoch %d : learning rate = %g\n' % (i+1, optimizer.lr))
        flog.flush()
    random.shuffle(train_data)
    for j in six.moves.range(len(train_data)):
        state = make_initial_state(1)
        iseq = train_data[j][0]
        olab[0] = train_data[j][1]
        for ivalue in iseq:
            ilab[0] = ivalue
            state = forward_one_step_no_output(ilab, state)
        ilab[0] = eos
        state, loss_i = forward_one_step(ilab, olab, state)
        accum_loss += loss_i
        cur_log_perp += loss_i.data.reshape(())
        if (n + 1) % 10000 == 0:
           now = time.time()
           throuput = 10000. / (now - cur_at)
           perp = math.exp(cuda.to_cpu(cur_log_perp) / 10000)
           print(' iter {} training perplexity: {:.2f} ({:.2f} iters/sec)'.format(
                    n + 1, perp, throuput))
           cur_at = now
           cur_log_perp.fill(0)
        
           #evaluate
           valid_hit, valid_count, valid_acc = evaluate(valid_data)
           print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
           if args.log != '':
              flog.write('  valid accuracy = %g (%d/%d)\n' % (valid_acc,valid_hit,valid_count))
              flog.flush()
        
        n += 1

        # Run truncated BPTT
        optimizer.zero_grads()
        accum_loss.backward()
        accum_loss.unchain_backward()  # truncate
        accum_loss = chainer.Variable(xp.zeros((), dtype=np.float32))

        optimizer.clip_grads(grad_clip)
        optimizer.update()

    print 'evaluate'
    now = time.time()
    valid_hit, valid_count, valid_acc = evaluate(valid_data)
    print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
    if args.log != '':
        flog.write('  valid accuracy = %g (%d/%d)\n' % (valid_acc,valid_hit,valid_count))
        flog.flush()

    if args.test != '':
        test_hit, test_count, test_acc = evaluate(test_data)
        print ' test accuracy: %g (%d/%d)' % (test_acc, test_hit, test_count)
        if args.log != '':
            flog.write('  test accuracy = %g (%d/%d)\n' % (test_acc,test_hit,test_count))
            flog.flush()
    sys.stdout.flush()
    #cur_at += time.time() - now  # skip time of evaluation

    if max_valid_acc <= valid_acc:
        print ' writing model params to', args.model
        if args.log != '':
            flog.write('  writing model params to %s\n' % args.model)
            flog.flush()
        model.to_cpu()
        fm = open(args.model, 'w')
        pickle.dump(ilabelset, fm);
        pickle.dump(olabelset, fm);
        pickle.dump(model, fm)
        fm.close()
        max_valid_acc = valid_acc
        model_saved = True
        if args.gpu >= 0:
            model.to_gpu()
    else:
        if model_saved==False:
            print ' reloading model params from', args.initial_model
            if args.log != '':
                flog.write('  reloading model params from %s\n' % args.initial_model)
                flog.flush()
            fm = open(args.initial_model, 'r')
        else:
            print ' reloading model params from', args.model
            if args.log != '':
                flog.write('  reloading model params from %s\n' % args.model)
                flog.flush()
            fm = open(args.model, 'r')
        ilabelset = pickle.load(fm)
        olabelset = pickle.load(fm)
        model = pickle.load(fm)
        temp,n_units = model.parameters[1].shape
        fm.close()
        if args.gpu >= 0:
            model.to_gpu()
        optimizer.lr *= args.learn_decay

    optimizer.setup(model)
    sys.stdout.flush()
    cur_at += time.time() - now  # skip time of evaluation and file I/O

# Save model params
if model_saved == False:
    print ' writing model params to', args.model
    if args.log != '':
        flog.write('  writing model params to %s\n' % args.model)
        flog.flush()
    model.to_cpu()
    fm = open(args.model, 'w')
    pickle.dump(ilabelset,fm)
    pickle.dump(olabelset,fm)
    pickle.dump(model, fm)
    fm.close()

print('done')
if args.log != '':
    flog.write('done\n')
    flog.close()

