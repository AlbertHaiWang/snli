#!/bin/bash

tag_type=base   # Dialog act only
#tag_type=rough # Dialog act + slot type

train=./data/if_jpn_${tag_type}_train.dat
dev=./data/if_jpn_${tag_type}_dev.dat
test=./data/if_jpn_${tag_type}_test.dat

n_units=100
learn_rate=0.5

mkdir -p model log
model=model/blstm${n_units}-${tag_type}-R${learn_rate}.txt
log=log/blstm${n_units}-${tag_type}-R${learn_rate}.log

blstm_train.py -g0 --train $train --valid $dev --test $test \
     -m $model -l $log
