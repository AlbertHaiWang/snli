#!/bin/bash

source ~/.bashrc

train=../snli_1.0/snli_1.0_train.txt
dev=../snli_1.0/snli_1.0_dev.txt
testt=../snli_1.0/snli_1.0_test.txt

n_units=100
learn_rate=0.5

model=model/lstm${n_units}-R${learn_rate}-role.txt
log=log/lstm${n_units}-R${learn_rate}-role.log

python lstm_train.py -g -1 --train $train --valid $dev --test $testt -m $model -l $log -u $n_units -R $learn_rate
