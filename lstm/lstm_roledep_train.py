#!/usr/bin/env python
"""Utterance classification using context-sensitive LSTM
   with role-dependent layers for chainer 1.4.0
"""
import argparse
import math
import sys
import time
import random
import os


from collections import defaultdict
import ontology_reader, dataset_walker, time, json
import misc

import numpy as np
import six

import chainer
from chainer import cuda
import chainer.functions as F
from chainer import optimizers
import pickle


parser = argparse.ArgumentParser()

parser.add_argument('--gpu', '-g', default=0, type=int,
                    help='GPU ID (negative value indicates CPU)')

# dstc 5 train, dev and test data
parser.add_argument('--train', default='dstc5_train.dat', type=str,
                    help='Filename of train data')
parser.add_argument('--valid', default='dstc5_dev.dat', type=str,
                    help='Filename of validation data')
parser.add_argument('--test', type=str,
                    help='Filename of test data')
parser.add_argument('--output', type=str,
                    help='Filename of prediction data')

# dstc 5 data and ontology 
parser.add_argument('--dataroot', default= '../data', type=str,
		    help='Will look for corpus in <destroot>/<dataset>/...')
parser.add_argument('--ontology', default= './config/ontology_dstc5.json', type=str,
		    help='JSON Ontology file')
parser.add_argument('--wordvector', default='', type=str,
		    help='word vector used')


# RNN model related
parser.add_argument('--initial-model', '-i', default='', type=str,
                    help='RNN model to be used')
parser.add_argument('--model', '-m', default='dialog.mdl', type=str,
                    help='RNN model file to be output')
parser.add_argument('--num-epochs', '-e', default=40, type=int,
                    help='Number of epochs')
parser.add_argument('--num-units', '-u', default=50, type=int,
                    help='Number of hidden units')
parser.add_argument('--num-projs', '-p', default=50, type=int,
                    help='Number of projection layer units')
parser.add_argument('--learn-rate', '-R', default=1.0, type=float,
                    help='Initial learning rate')
parser.add_argument('--learn-decay', '-D', default=0.5, type=float,
                    help='Decaying ratio of learning rate')
parser.add_argument('--log', '-l', default='', type=str,
                    help='Log filename')


args = parser.parse_args()
xp = cuda.cupy if args.gpu >= 0 else np

# open log file
if args.log != '':
    flog = open(args.log, 'w')

random.seed(1)
np.random.seed(1)

n_epochs = args.num_epochs  # number of epochs
n_units = args.num_units  # number of units per layer
n_projs = args.num_projs  # number of units per layer
grad_clip = 5    # gradient norm threshold to clip
tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets() # tag set is all the tag set we need predict

print 'Model file:', args.model

# Load text data
def load_data(filename, train=True):
    global ilabelset # ilabelset is the word set
    global olabelset # olabelset is the observation set
    dataset = []
    sequence = []
    print 'Loading', filename
    for s in open(filename).readlines():
        data = s.split()
        temp = data[0]
        if temp == '<eod>':
            dataset.append(sequence)
            sequence = []
        else:
            frame = data[-2]
            topic = data[-1]
 	    olabel = temp[2:]
            ilabels = data[1:-2]	
            seqdata = np.ndarray((len(ilabels),), dtype=np.int32)
            for i, ilabel in enumerate(ilabels):
                if ilabel in ilabelset:
                    seqdata[i] = ilabelset[ilabel]
                else:
                    if train==True:
                        seqdata[i] = len(ilabelset)
                        ilabelset[ilabel] = len(ilabelset)
                    else:
                        seqdata[i] = unk
            if olabel in olabelset:
                olabel_id = olabelset[olabel]
            else:
                if train==True:
                    olabel_id = len(olabelset)
                    olabelset[olabel] = len(olabelset)
                else:
                    olabel_id = unk

            if temp.find("c:") >= 0:
                role = 1
	    else:
		role = 2	
            sequence.append((seqdata, olabel_id, role, frame, topic, ilabels))
    return dataset


def get_wordvector(wordvectorfile, n_word, n_dimension):

    print 'Loading word vectors', wordvectorfile     
    word_vector = defaultdict(set)
    embedding_matrix = np.zeros(shape =(n_word, n_dimension))

    idx = 0	
    for s in open(wordvectorfile).readlines():
        data = s.split()
        word = data[0]
        vector = data[1:]
        word_vector[word] = vector

    mean_vector = word_vector[word]  #random assign one, might change one

		
    for word, idx in ilabelset.items():
	if idx != "<eos>" and idx != "<unk>":
	   if word in word_vector.keys():
              embedding_matrix[idx] = word_vector[word]
	   else:
	      embedding_matrix[idx] = mean_vector

    return embedding_matrix
	
# Load text data
def load_data2(filename, train=True):
    global ilabelset # ilabelset is the word set
    global olabelset # olabelset is the observation set
    dataset = []
    sequence = []
    print 'Loading', filename
    for s in open(filename).readlines():
        data = s.split()
        temp = data[0]
        frame = data[-2]
        topic = data[-1]
        if temp == '<eod>':
            dataset.append(sequence)
            sequence = []
        else:
            olabel = topic
            ilabels = data[1:-2]
            seqdata = np.ndarray((len(ilabels),), dtype=np.int32)
            for i, ilabel in enumerate(ilabels):
                if ilabel in ilabelset:
                    seqdata[i] = ilabelset[ilabel]
                else:
                    if train==True:
                        seqdata[i] = len(ilabelset)
                        ilabelset[ilabel] = len(ilabelset)
                    else:
                        seqdata[i] = unk
            if olabel in olabelset:
                olabel_id = olabelset[olabel]
            else:
                if train==True:
                    olabel_id = len(olabelset)
                    olabelset[olabel] = len(olabelset)
                else:
                    olabel_id = unk

            if temp.find("c:") >= 0:
                role = 1
            else:
                role = 2
            sequence.append((seqdata, olabel_id, role, frame, topic))
    return dataset


#def make_datafile(dataset, dataroot, tagsets, fileoutput, translate):

#   fp = open(fileoutput, 'wt+')
#    for call in dataset:
#	firstline=False
#        for (log, translations, label) in call:
#	     if translate == False:
#                topic, speaker, transcript, framelabel, frame = misc.get_framelabel(label, log, tagsets)
#	     else:
#                topic, speaker, transcript, framelabel, frame = misc.get_framelabel_translate(label, log, translations, tagsets)
#             line = misc.convert2string(topic, speaker, transcript, framelabel, frame)
#             if frame == 'B' and firstline == True:
#                fp.write("<eod> <eod>\n")
#	     fp.write(line)
#	     firstline = True

#    fp.write("<eod> <eod>")	 
#    fp.close()

# prepare the new data now
#prepare the RNN dataset from label.json and log.json


# train
#dataset = dataset_walker.dataset_walker(args.train, dataroot=args.dataroot, labels=True, translations = True)
#train_temp = args.train + ".dat"
#make_datafile(dataset, args.dataroot, tagsets, train_temp, False)
# dev
#if args.valid != '':
#   dataset = dataset_walker.dataset_walker(args.valid, dataroot=args.dataroot, labels=True, translations = True)
#   valid_temp = args.valid + ".dat"
#   make_datafile(dataset, args.dataroot, tagsets, valid_temp, True)

# test
#if args.test != '':
#   dataset = dataset_walker.dataset_walker(args.test, dataroot=args.dataroot, labels=True, translations = True)
#   test_temp = args.test + ".dat"
#   make_datafile(dataset, args.dataroot, tagsets, test_temp)



# Prepare RNN model and load data
if args.initial_model != '':
    print 'Loading model params from', args.initial_model
    fm = open(args.initial_model, 'r')
    ilabelset = pickle.load(fm)
    olabelset = pickle.load(fm)
    model = pickle.load(fm)
    temp,n_units = model.parameters[1].shape
    fm.close()
    eos = ilabelset['<eos>']
    unk = ilabelset['<unk>']
    train_data = load_data(args.train, train=True) # now read the new train data
    valid_data = load_data(args.valid, train=False)  # now read the new dev data
    test_data = load_data(args.valid, train=False)  # now read the new test data
else:
    ilabelset = {0:'<eos>', 1:'<unk>'}
    olabelset = {0:'<eos>', 1:'<unk>'}
    eos = 0
    unk = 1
    train_data = load_data(args.train, train=True)
    valid_data = load_data(args.valid, train=False)
    test_data = load_data(args.valid, train=False)

    #embedm = F.embed_id(len(ilabelset), n_projs)

    model = chainer.FunctionSet(embed = F.EmbedID(len(ilabelset), n_projs),
                                l1_x=F.Linear(n_projs, 4 * n_units),
                                l1_h=F.Linear(n_units, 4 * n_units),
                                l2_x=F.Linear(n_projs, 4 * n_units),
                                l2_h=F.Linear(n_units, 4 * n_units),
                                l3=F.Linear(n_units, len(olabelset)))

print '#ilabel =', len(ilabelset)
print '#olabel =', len(olabelset)
print 'train:', len(train_data), 'dialogs'
print 'valid:', len(valid_data), 'dialogs'
print 'test:', len(test_data), 'dialogs'


if args.wordvector != '':
   embedding = get_wordvector(args.wordvector, len(ilabelset), 50)

for param in model.parameters:
    if param.shape == (len(ilabelset), n_projs):
       param[:] = embedding    # with word embedding
    else:
       param[:] = np.random.uniform(-0.1, 0.1, param.shape)


if args.gpu >= 0:
    cuda.check_cuda_available()
    cuda.get_device(args.gpu).use()
    xp.random.seed(1)
    model.to_gpu()

# Neural net architecture
def forward_one_step1(x_data, y_data, state, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    t = chainer.Variable(xp.asarray(y_data), volatile=not train)
    h0 = model.embed(x)
    #print h0	
    h1_in = model.l1_x(F.dropout(h0, train=train)) + model.l1_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    y = model.l3(F.dropout(h1, train=train))
    state = {'c1': c1, 'h1': h1}
    if train==True:
        return state, F.softmax_cross_entropy(y, t)
    else:
        return state, F.softmax(y)

def forward_one_step2(x_data, y_data, state, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    t = chainer.Variable(xp.asarray(y_data), volatile=not train)
    h0 = model.embed(x)
    h1_in = model.l2_x(F.dropout(h0, train=train)) + model.l2_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    y = model.l3(F.dropout(h1, train=train))
    state = {'c1': c1, 'h1': h1}
    if train==True:
        return state, F.softmax_cross_entropy(y, t)
    else:
        return state, F.softmax(y)

def forward_one_step_no_output1(x_data, state, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    h0 = model.embed(x)
    h1_in = model.l1_x(F.dropout(h0, train=train)) + model.l1_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    state = {'c1': c1, 'h1': h1}
    return state

def forward_one_step_no_output2(x_data, state, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    h0 = model.embed(x)
    h1_in = model.l2_x(F.dropout(h0, train=train)) + model.l2_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    state = {'c1': c1, 'h1': h1}
    return state

def make_initial_state(batchsize=1, train=True):
    return {name: chainer.Variable(xp.zeros((batchsize, n_units),
                                             dtype=np.float32),
                                   volatile=not train)
            for name in ('c1', 'h1')}


# Evaluation routine
def evaluate(eval_data):
    hit = 0
    count = 0
    for j in six.moves.range(len(eval_data)):
        state = make_initial_state(1, train=False)
        for k in six.moves.range(len(eval_data[j])):
            iseq = eval_data[j][k][0]
            olab[0] = eval_data[j][k][1]
            role = eval_data[j][k][2]
	    frame = eval_data[j][k][3]  # ignore the one with "O"
	    if frame != "O":		    
               if role==1:
                  for ivalue in iseq:
                      ilab[0] = ivalue
		      #print ilab[0]	
                      state = forward_one_step_no_output1(ilab, state, train=False)
                  ilab[0] = eos
                  state, prob = forward_one_step1(ilab, olab, state, train=False) 
               else:
                  for ivalue in iseq:
                      ilab[0] = ivalue
                      state = forward_one_step_no_output2(ilab, state, train=False)
                  ilab[0] = eos
                  state, prob = forward_one_step2(ilab, olab, state, train=False) 
               
               pp = cuda.to_cpu(prob.data)
               result = np.argmax(pp[0])
               
               if result == olab[0]:
                  hit += 1
               count += 1
    
    acc = (float(hit)/count) * 100.0
    return hit, count, acc

def evaluate_all(eval_data):
    hit = 0
    count = 0
    for j in six.moves.range(len(eval_data)):
        state = make_initial_state(1, train=False)
        for k in six.moves.range(len(eval_data[j])):
            iseq = eval_data[j][k][0]
            olab[0] = eval_data[j][k][1]
            role = eval_data[j][k][2]
            frame = eval_data[j][k][3]  # ignore the one with "O"
           
            if role==1:
               for ivalue in iseq:
                   ilab[0] = ivalue
                   state = forward_one_step_no_output1(ilab, state, train=False)
               ilab[0] = eos
               state, prob = forward_one_step1(ilab, olab, state, train=False)
            else:
               for ivalue in iseq:
                   ilab[0] = ivalue
                   state = forward_one_step_no_output2(ilab, state, train=False)
               ilab[0] = eos
               state, prob = forward_one_step2(ilab, olab, state, train=False)
            
            pp = cuda.to_cpu(prob.data)
            result = np.argmax(pp[0])
            #print result, pp[0][result], ' - ', olab[0], pp[0][olab[0]]
            if result == olab[0]:
               hit += 1
            count += 1

    acc = (float(hit)/count) * 100.0
    return hit, count, acc


def write_prediction(eval_data, filename):

    fp = open(filename, "wt+")

    for j in six.moves.range(len(eval_data)):
        state = make_initial_state(1, train=False)
        for k in six.moves.range(len(eval_data[j])):
            
            iseq = eval_data[j][k][0]
            olab[0] = eval_data[j][k][1]
            role = eval_data[j][k][2]
            frame = eval_data[j][k][3]
	    topic = eval_data[j][k][4]
	    utter = eval_data[j][k][5]

            if frame != "O":
               if role==1:
                  for ivalue in iseq:
                      ilab[0] = ivalue
                      #print ilab[0]
                      state = forward_one_step_no_output1(ilab, state, train=False)
                  ilab[0] = eos
                  state, prob = forward_one_step1(ilab, olab, state, train=False)
               else:
                  for ivalue in iseq:
                      ilab[0] = ivalue
                      state = forward_one_step_no_output2(ilab, state, train=False)
                  ilab[0] = eos
                  state, prob = forward_one_step2(ilab, olab, state, train=False)

               pp = cuda.to_cpu(prob.data)
               result = np.argmax(pp[0])
               pred = list(olabelset)[result]               
            else:   
               pred = "no-tag"
 	    
            utters = ""
	    for i in range(len(utter)):
		utters += utter[i] + " "
	    utters = utters[:-1]         
            line = misc.convert2string_result(role, pred, utters, frame, topic)
            fp.write(line)
    
        fp.write("<eod> <eod>\n")  	
    
    fp.close()


#def exist_value(tagsets, topic, slot, value):

#    if value in tagsets[topic][slot]:
#       return True
#    return False

#def slot_value(ss):
#    words = ss.split("_")
#    result = 0	
#    for i in range(len(words)):
#	if words[i].isupper():
#	   result = result + 1
    
#    if result == len(words):
#       return True
#    else:
#       return False	
   		
#def convertresult(result, tagsets, topic):
     
#    result = result[-len(result) + 2:]
#    tracker_result = {}
#    words = result.split("+")	
#    for i in range(len(words)):
#	v = words[i]
#	if slot_value(v):
#	   slot = v
#	   if slot not in tracker_result:
#              tracker_result[slot] = []
#	else:
#	   v = v.replace("-", " ")
#	   if v not in tracker_result[slot]:
#	      tracker_result[slot].append(v)
    
#    for slot, v in tracker_result.items():
#	if slot not in tagsets[topic]:
#	   del tracker_result[slot]
#	else:
#           for value in tracker_result[slot]:
#	       if value not in tagsets[topic][slot]:  # we need to check this one now since TO:[{"slot": "source": format}]	
#	          tracker_result[slot].remove(value)
#           if len(tracker_result[slot]) == 0:
#	      del tracker_result[slot]

#   return tracker_result	


# make tracker file
#def make_trackerfile(data, datasetname, dataset, filename):
    
#    track_file = open(filename, "wb+")
#    track = {"sessions":[]}
#    track["dataset"] = datasetname

#    call_idx = 0
#    utter_idx = -1
#    total_idx = 0	
#    len_call = len(list(dataset)[0])  # initial length of those calls

#    call = list(dataset)[call_idx]
#    this_session = {"session_id": call.log["session_id"], "utterances": []}
    
#    start_time = time.time()
#    for j in six.moves.range(len(data)):
        
#	state = make_initial_state(1, train = False)
	
#        for k in six.moves.range(len(data[j])):
#            iseq = data[j][k][0] 	# sequence
#            olab[0] = data[j][k][1] 	# label
#            role = data[j][k][2] 	# role
#	    frame = data[j][k][3]	# frame label
#	    topic = data[j][k][4]	# topic  
#	    if frame != "O":	
#               if role==1:
#                  for ivalue in iseq:
#                      ilab[0] = ivalue
#                      state = forward_one_step_no_output1(ilab, state, train=False)
#                  ilab[0] = eos
#                  state, prob = forward_one_step1(ilab, olab, state, train=False)
#               else:
#                  for ivalue in iseq:
#                      ilab[0] = ivalue
#                      state = forward_one_step_no_output2(ilab, state, train=False)
#                  ilab[0] = eos
#                  state, prob = forward_one_step2(ilab, olab, state, train=False)
#               pp = cuda.to_cpu(prob.data)
#               result = np.argmax(pp[0])
#	       total_idx = total_idx + 1
	
	       #print total_idx, len_call, call_idx, len(call), utter_idx 	
#	       if total_idx > len_call:	
            	  # now change the forma	  
#                  track["sessions"].append(this_session)  # session
#		  call_idx = call_idx + 1
#		  call = list(dataset)[call_idx] 	# change the new call now
#		  utter_idx = 0 			# update the utter_idx
#	          len_call += len(call) 		# update the len_call now
#		  this_session = {"session_id": call.log["session_id"], "utterances": []}
#	       else:
#                  utter_idx += 1

	       
	       #print call_idx, utter_idx, topic

               #total_idx = total_idx + 1	
#	       utter = list(call)[utter_idx][0]
#	       tracker_result = {'utter_index': utter['utter_index']}
 	       #print tracker_result['utter_index']
	
#	       if list(olabelset)[result] == "no-tag":
#		  tracker_result['frame_label'] = {}
#	       else:	
#	          rresult = convertresult(list(olabelset)[result], tagsets, topic)

#		  tracker_result['frame_label'] = rresult
#	          if tracker_result is not None:
#                    this_session["utterances"].append(tracker_result) # each turn, we have the tracker
#            else:
#               total_idx = total_idx + 1
#               if total_idx > len_call:
                  # now change the forma
#                  call_idx = call_idx + 1
#                  call = list(dataset)[call_idx]        # change the new call now
#                  utter_idx = 0                         # update the utter_idx
#                  len_call += len(call)                 # update the len_call now	
#		  track["sessions"].append(this_session)  # session
#                  this_session = {"session_id": call.log["session_id"], "utterances": []}
#               else:
#		  utter_idx += 1

               #print "length 2: %d \n" % len(list(dataset)[call_idx])
	       #print "total 2 %d \n" % total_idx
               #print "call idx 2:%d utter idx: %d \n" % (call_idx, utter_idx)
#               utter = list(call)[utter_idx][0]
#	       tracker_result = {'utter_index': utter['utter_index']} 
#               this_session["utterances"].append(tracker_result)
	       
    
    # print the final session    
#    track["sessions"].append(this_session)
 
#    end_time = time.time()
#    elapsed_time = end_time - start_time
#    track['wall_time'] = elapsed_time	
    # write it to the file		
#    json.dump(track, track_file, indent=4)
#    track_file.close()


# Evaluation routine
#def evaluate_dstc(dataset, data, ontology, tracker):
    
    # run the dstc evaluation code
#    str_command = "python ../check_main.py --dataset %s --dataroot %s --ontology %s --trackfile %s" % (dataset, data, ontology, tracker)
#    os.system(str_command)
#    scorefile="%s.score.csv" % tracker
#    str_command = "python ../score_main.py --dataset %s --dataroot %s --ontology %s --trackfile %s --scorefile %s" % (dataset, data, ontology, tracker, scorefile)
#    os.system(str_command)
#    str_command = "python ../report_main.py --scorefile %s"% scorefile
#    os.system(str_command)


############################################
print '----------------'
print 'Start training'
print '----------------'
sys.stdout.flush()
if args.log != '':
    flog.write('Start training\n')
    flog.flush()

# Setup optimizer
optimizer = optimizers.SGD(lr=args.learn_rate)
optimizer.setup(model)

#random.shuffle(train_batchset)
cur_log_perp = xp.zeros(())
epoch = 0
start_at = time.time()
cur_at = start_at
accum_loss = chainer.Variable(xp.zeros((), dtype=np.float32))

model_saved = False
ilab = np.zeros((1,), dtype=np.int32)
olab = np.zeros((1,), dtype=np.int32)


#dataset = dataset_walker.dataset_walker(args.train, dataroot=args.dataroot, labels = False, translations = False)
#make_trackerfile(train_data, args.train, dataset, "train.json")
#evaluate_dstc(args.train, args.dataroot, args.ontology, "train.json")


# initial evaluation
#train_hit, train_count, train_acc = evaluate(train_data)
#print ' train accuracy: %g (%d/%d)' % (train_acc, train_hit, train_count)
#if args.log != '':
#   flog.write('  initial train accuracy = %g (%d/%d)\n' % (train_acc, train_hit, train_count))
#   flog.flush()


# only in case of we have test data
if args.valid != '':
    valid_hit, valid_count, valid_acc = evaluate(valid_data)
    print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
    valid_hit_all, valid_count_all, valid_acc_all = evaluate_all(valid_data)
    print ' valid accuracy all: %g (%d/%d)' % (valid_acc_all, valid_hit_all, valid_count_all)

    if args.log != '':
        flog.write('  initial valid accuracy = %g (%d/%d)\n' % (valid_acc, valid_hit, valid_count))
        flog.flush()
max_valid_acc = valid_acc


# only in case of we have test data
if args.test != '':
    test_hit, test_count, test_acc = evaluate(test_data)
    print ' test accuracy: %g (%d/%d)' % (test_acc, test_hit, test_count)
    if args.log != '':
        flog.write('  initial test accuracy = %g (%d/%d)\n' % (test_acc,test_hit,test_count))
        flog.flush()


write_prediction(valid_data, args.output)

n = 0

for i in six.moves.range(n_epochs):
    print '----------------'
    print 'Epoch %d : learning rate = %g' % (i+1, optimizer.lr)
    if args.log != '':
        flog.write('Epoch %d : learning rate = %g\n' % (i+1, optimizer.lr))
        flog.flush()
    random.shuffle(train_data)
    for j in six.moves.range(len(train_data)):
        state = make_initial_state(1)
        for k in six.moves.range(len(train_data[j])):
            iseq = train_data[j][k][0]
            olab[0] = train_data[j][k][1]
            role = train_data[j][k][2]
            frame = train_data[j][k][3] # don't skip the frame "O" for training now
            if frame != "O":
               if role==1:
                  for ivalue in iseq:
                      ilab[0] = ivalue
                      state = forward_one_step_no_output1(ilab, state)
                  ilab[0] = eos
                  state, loss_i = forward_one_step1(ilab, olab, state) 
               else:
                  for ivalue in iseq:
                      ilab[0] = ivalue
                      state = forward_one_step_no_output2(ilab, state)
                  ilab[0] = eos
                  state, loss_i = forward_one_step2(ilab, olab, state) 

               accum_loss += loss_i
               cur_log_perp += loss_i.data.reshape(())
               if (n + 1) % 1000 == 0:
                  now = time.time()
                  throuput = 10000. / (now - cur_at)
                  perp = math.exp(cuda.to_cpu(cur_log_perp) / 10000)
                  print(' iter {} training perplexity: {:.2f} ({:.2f} iters/sec)'.format(n + 1, perp, throuput))
                  cur_at = now
                  cur_log_perp.fill(0)
               n += 1
        
        # Run truncated BPTT
        optimizer.zero_grads()
        accum_loss.backward()
        accum_loss.unchain_backward()  # truncate
        accum_loss = chainer.Variable(xp.zeros((), dtype=np.float32))
        optimizer.clip_grads(grad_clip)
        optimizer.update()


    print '--------------evaluate using lstm------------------'
    now = time.time()

    #train_hit, train_count, train_acc = evaluate(train_data)
    #print ' train accuracy: %g (%d/%d)' % (train_acc, train_hit, train_count)
    #if args.log != '':
    #   flog.write('  train accuracy = %g (%d/%d)\n' % (train_acc, train_hit, train_count))
    #   flog.flush()

    if args.valid != '':
       valid_hit, valid_count, valid_acc = evaluate(valid_data)
       print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
       valid_hit_all, valid_count_all, valid_acc_all = evaluate_all(valid_data)
       print ' valid accuracy all: %g (%d/%d)' % (valid_acc_all, valid_hit_all, valid_count_all)
	
       if args.log != '':
          flog.write('  valid accuracy = %g (%d/%d)\n' % (valid_acc,valid_hit,valid_count))
          flog.flush()

    if args.test != '':
       test_hit, test_count, test_acc = evaluate(test_data)
       print ' test accuracy: %g (%d/%d)' % (test_acc, test_hit, test_count)
       if args.log != '':
          flog.write('  test accuracy = %g (%d/%d)\n' % (test_acc,test_hit,test_count))
          flog.flush()
    
    sys.stdout.flush()
    cur_at += time.time() - now  # skip time of evaluation
    

    # evaluate using the DSTC5 metric     
    # print '--------------------evaluate using dstc5 metric--------------'

    # train-- take too much time
    #dataset = dataset_walker.dataset_walker(args.train, dataroot=args.dataroot, labels = False, translations = False)
    #make_trackerfile(train_data, args.train, dataset, "train.json")
    #evaluate_dstc(args.train, args.dataroot, args.ontology, "trjsonn")
    
    # validation 
    #if args.valid != '':    
    #   dataset = dataset_walker.dataset_walker(args.valid, dataroot=args.dataroot, labels = False, translations = True)
    #   make_trackerfile(valid_data, args.valid, dataset, "validation.json")
    #   evaluate_dstc(args.valid, args.dataroot, args.ontology, "validation.json")
   
    # test
    #if args.test != '':
    #   dataset = dataset_walker.dataset_walker(args.test, dataroot=args.dataroot, labels = False, translations = True)
    #   make_trackerfile(test_data, args.test, dataset, "test.json")
    #   evaluate_dstc(args.test, args.dataroot, args.ontology, "test.json")


    # update the model via comparing with the highest accuracy  
    if max_valid_acc <= valid_acc:
        print ' writing model params to', args.model
        if args.log != '':
            flog.write('  writing model params to %s\n' % args.model)
            flog.flush()
        model.to_cpu()
        fm = open(args.model, 'w')
        pickle.dump(ilabelset, fm);
        pickle.dump(olabelset, fm);
        pickle.dump(model, fm)
        fm.close()
        max_valid_acc = valid_acc
        model_saved = True
        if args.gpu >= 0:
            model.to_gpu()
    else:
        if model_saved==False:
            print ' reloading model params from', args.initial_model
            if args.log != '':
                flog.write('  reloading model params from %s\n' % args.initial_model)
                flog.flush()
            fm = open(args.initial_model, 'r')
        else:
            print ' reloading model params from', args.model
            if args.log != '':
                flog.write('  reloading model params from %s\n' % args.model)
                flog.flush()
            fm = open(args.model, 'r')
        ilabelset = pickle.load(fm)
        olabelset = pickle.load(fm)
        model = pickle.load(fm)
        temp,n_units = model.parameters[1].shape
        fm.close()
        if args.gpu >= 0:
            model.to_gpu()
        optimizer.lr *= args.learn_decay

    optimizer.setup(model)
    sys.stdout.flush()
    cur_at += time.time() - now  # skip time of evaluation and file I/O


# Save model params
if model_saved == False:
    print ' writing model params to', args.model
    if args.log != '':
        flog.write('  writing model params to %s\n' % args.model)
        flog.flush()
    model.to_cpu()
    fm = open(args.model, 'w')
    pickle.dump(ilabelset,fm)
    pickle.dump(olabelset,fm)
    pickle.dump(model, fm)
    fm.close()

print('done')
if args.log != '':
    flog.write('done\n')
    flog.close()

write_prediction(eval_data, args.output)
