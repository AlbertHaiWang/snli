#!/usr/bin/env python
"""Utterance classification using a context-sensitive attention model
   with role-dependent layers for chainer 1.4.0
"""
import argparse
import math
import sys
import time
import random

import numpy as np
import six

import chainer
from chainer import cuda
import chainer.functions as F
from chainer import optimizers
import operator
import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--gpu', '-g', default=-1, type=int,
                    help='GPU ID (negative value indicates CPU)')
parser.add_argument('--train', default='data/if_jpn_base_train.dat', type=str,
                    help='Filename of train data')
parser.add_argument('--valid', default='data/if_jpn_base_dev.dat', type=str,
                    help='Filename of validation data')
parser.add_argument('--test', default='data/if_jpn_base_test.dat', type=str,
                    help='Filename of test data')
parser.add_argument('--initial-model', '-i', default='', type=str,
                    help='RNN model to be used')
parser.add_argument('--model', '-m', default='model/lstm.mdl', type=str,
                    help='RNN model file to be output')
parser.add_argument('--num-epochs', '-e', default=40, type=int,
                    help='Number of epochs')
parser.add_argument('--num-units', '-u', default=100, type=int,
                    help='Number of hidden units')
parser.add_argument('--num-projs', '-p', default=100, type=int,
                    help='Number of projection layer units')
parser.add_argument('--num-attns', '-a', default=100, type=int,
                    help='Number of attention layer units')
parser.add_argument('--learn-rate', '-R', default=0.5, type=float,
                    help='Initial learning rate')
parser.add_argument('--learn-decay', '-D', default=0.5, type=float,
                    help='Decaying ratio of learning rate')
parser.add_argument('--overlap', '-o', default=1, type=int,
                    help='Overlap of attention window')
parser.add_argument('--no-future', '-n', default=False, action='store_true',
                    help='Do not use future info')
parser.add_argument('--log', '-l', default='', type=str,
                    help='Log filename')
args = parser.parse_args()
xp = cuda.cupy if args.gpu >= 0 else np

# open log file
if args.log != '':
    flog = open(args.log, 'w')

random.seed(2)
np.random.seed(2)

n_epochs = args.num_epochs  # number of epochs
n_units = args.num_units  # number of units per layer
n_projs = args.num_projs  # number of units per layer
n_attns = args.num_attns  # number of units per layer
grad_clip = 5    # gradient norm threshold to clip

print 'Model file:', args.model

# Load text data
def load_data(filename, train=True):
    global ilabelset
    global olabelset
    dataset = []
    sequence = []
    print 'Loading', filename
    for s in open(filename).readlines():
        data = s.split()
        olabel = data[0]
        if olabel == '<eod>':
            dataset.append(sequence)
            sequence = []
        else:
            ilabels = data[1:]
            seqdata = np.ndarray((len(ilabels),), dtype=np.int32)
            for i, ilabel in enumerate(ilabels):
                if ilabel in ilabelset:
                    seqdata[i] = ilabelset[ilabel]
                else:
                    if train==True:
                        seqdata[i] = len(ilabelset)
                        ilabelset[ilabel] = len(ilabelset)
                    else:
                        seqdata[i] = unk
            if olabel in olabelset:
                olabel_id = olabelset[olabel]
            else:
                if train==True:
                    olabel_id = len(olabelset)
                    olabelset[olabel] = len(olabelset)
                else:
                    olabel_id = unk

            if olabel.find("c:") >= 0:
                role = 0
            else:
                role = 1

            sequence.append((seqdata, olabel_id, role))
    return dataset

# Prepare RNN model and load data
if args.initial_model != '':
    print 'Lording model params from', args.initial_model
    fm = open(args.initial_model, 'r')
    ilabelset = pickle.load(fm)
    olabelset = pickle.load(fm)
    model = pickle.load(fm)
    temp,n_units = model.parameters[2].shape
    fm.close()
    eos = ilabelset['<eos>']
    unk = ilabelset['<unk>']
    train_data = load_data(args.train, train=True)
    valid_data = load_data(args.valid, train=False)
    test_data = load_data(args.test, train=False)
else:
    ilabelset = {'<eos>':0, '<unk>':1}
    olabelset = {'<eos>':0, '<unk>':1}
    eos = 0
    unk = 1
    train_data = load_data(args.train, train=True)
    valid_data = load_data(args.valid, train=False)
    test_data = load_data(args.test, train=False)
    model = chainer.FunctionSet(embed=F.EmbedID(len(ilabelset), n_projs),
                                l1Af_x=F.Linear(n_projs, 4 * n_units),
                                l1Af_h=F.Linear(n_units, 4 * n_units),
                                l1Ab_x=F.Linear(n_projs, 4 * n_units),
                                l1Ab_h=F.Linear(n_units, 4 * n_units),
                                l1Bf_x=F.Linear(n_projs, 4 * n_units),
                                l1Bf_h=F.Linear(n_units, 4 * n_units),
                                l1Bb_x=F.Linear(n_projs, 4 * n_units),
                                l1Bb_h=F.Linear(n_units, 4 * n_units),
                                atV=F.Linear(2 * n_units, n_attns),
                                atW=F.Linear(n_units, n_attns),
                                atw=F.Linear(n_attns, 1),
                                l1d_x=F.Linear(2 * n_units, 4*n_units),
                                l1d_h=F.Linear(n_units, 4*n_units),
                                l1d_y=F.Linear(len(olabelset), 4*n_units),
                                l2d=F.Linear(n_units, len(olabelset)))
    for param in model.parameters:
        param[:] = np.random.uniform(-0.1, 0.1, param.shape)

print '#ilabel =', len(ilabelset)
print '#olabel =', len(olabelset)
print 'train:', len(train_data), 'dialogs'
print 'valid:', len(valid_data), 'dialogs'
print 'test:', len(test_data), 'dialogs'

if args.gpu >= 0:
    cuda.check_cuda_available()
    cuda.get_device(args.gpu).use()
    xp.random.seed(2)
    model.to_gpu()

# Neural computations
def make_initial_state(batchsize=1, train=True):
    return {name: chainer.Variable(xp.zeros((batchsize, n_units),
                                             dtype=np.float32),
                                   volatile=not train)
            for name in ('c1', 'h1')}

def embed(x_data, train=True):
    x = chainer.Variable(xp.asarray(x_data), volatile=not train)
    return model.embed(x)

def forward_one_stepA(x, state, train=True):
    h1_in = model.l1Af_x(F.dropout(x, train=train)) + model.l1Af_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    return {'c1': c1, 'h1': h1}

def forward_one_stepB(x, state, train=True):
    h1_in = model.l1Bf_x(F.dropout(x, train=train)) + model.l1Bf_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    return {'c1': c1, 'h1': h1}

def backward_one_stepA(x, state, train=True):
    h1_in = model.l1Ab_x(F.dropout(x, train=train)) + model.l1Ab_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    return {'c1': c1, 'h1': h1}

def backward_one_stepB(x, state, train=True):
    h1_in = model.l1Bb_x(F.dropout(x, train=train)) + model.l1Bb_h(state['h1'])
    c1, h1 = F.lstm(state['c1'], h1_in)
    return {'c1': c1, 'h1': h1}

def blstm_encode(data, no_future=False, train=True):
    global ilab
    h0 = []
    h1f = []
    h1b = []
    h1 = []
    # projection
    ilab[0] = eos # put <eos> at the begining of the dialog
    h0.append( embed( ilab, train=train) )
    for k in six.moves.range(len(data)):
        for ivalue in data[k][0]:
            ilab[0] = ivalue
            h0.append( embed( ilab, train=train) )
        ilab[0] = eos # append <eos> at the end of each utterance
        h0.append( embed( ilab, train=train) )
    # forward path
    state = make_initial_state(1, train=train)
    t = 0
    for k in six.moves.range(len(data)):
        # select role-dependent layer
        forward_one_step = forward_one_stepA if data[k][2]==0 else forward_one_stepB
        # for the first <eos>
        if k==0:
            state = forward_one_step(h0[t], state, train=train)
            h1f.append(state['h1'])
            t += 1
        # within a sentence
        for j in six.moves.range(len(data[k][0])):
            state = forward_one_step(h0[t], state, train=train)
            h1f.append(state['h1'])
            t += 1
        # for sentence end
        state = forward_one_step(h0[t], state, train=train)
        h1f.append(state['h1'])
        t += 1

    # backward path
    state = make_initial_state(1, train=train)
    for k in six.moves.range(len(data)-1,-1,-1):
        # select role-dependent layer
        backward_one_step = backward_one_stepA if data[k][2]==0 else backward_one_stepB
        # for the final <eos>
        if k==len(data)-1:
            t -= 1
            state = backward_one_step(h0[t], state, train=train)
            h1b.insert(0, state['h1'])
        # within a sentence
        for j in six.moves.range(len(data[k][0])):
            t -= 1
            state = backward_one_step(h0[t], state, train=train)
            h1b.insert(0, state['h1'])
        # clear future context
        if no_future==True and t > 1:
            state = make_initial_state(1, train=train)
        # for sentence begin
        t -= 1
        state = backward_one_step(h0[t], state, train=train)
        h1b.insert(0, state['h1'])

    # concatenation
    h1 = [ F.concat((h1f[t], h1b[t])) for t in six.moves.range(len(h0)) ]
    del h0, h1f, h1b
    return h1

def attention(h1, s1, role, train=True):
    vh1 = [model.atV(F.dropout(h1[i], train=train)) for i in six.moves.range(len(h1))]
    ws1 = model.atW(F.dropout(s1, train=train))
    e = [F.exp(model.atw(F.tanh(vh1[i] + ws1))) for i in six.moves.range(len(vh1))]
    esum = e[0]
    for i in six.moves.range(1,len(e)):
        esum += e[i]
    g = F.broadcast_to(e[0] / esum, h1[0].data.shape) * h1[0]
    for i in six.moves.range(1,len(e)):
        g += F.broadcast_to(e[i] / esum, h1[i].data.shape) * h1[i]
    del vh1,e
    return g

def decode_one_step(x, s, y, train=True):
    h1_in = model.l1d_x(F.dropout(x, train=train)) + model.l1d_h(s['h1']) + model.l1d_y(y)
    c1, h1 = F.lstm(s['c1'], h1_in)
    return {'c1': c1, 'h1': h1}

def cross_entropy(x, t_data):
    t = chainer.Variable(xp.asarray(t_data), volatile=False)
    y = model.l2d(F.dropout(x, train=True))
    return F.softmax_cross_entropy(y, t), F.softmax( y )

def classify(x):
    prob = F.softmax( model.l2d(x) )
    pp = cuda.to_cpu(prob.data)
    return np.argmax(pp[0]), prob

# Evaluation routine
def evaluate(eval_data, overlap, no_future=False):
    global olab
    # get boundary info
    bps = []
    for j in six.moves.range(len(eval_data)):
        m = 1
        bp = []
        for k in six.moves.range(len(eval_data[j])):
            bp.append(m)
            m += (len(eval_data[j][k][0]) + 1) # '+1' is added for <eos>
        bp.append(m)
        bps.append(bp)

    hit = 0
    count = 0
    for j in six.moves.range(len(eval_data)):
        # blstm encoding for j-th dialog
        h1 = blstm_encode(eval_data[j], no_future=no_future, train=False)
        # compute accuracy
        s1 = make_initial_state(1, train=False)
        y = chainer.Variable(xp.zeros((1, len(olabelset)), dtype=np.float32),
                                   volatile=True)
        for k in six.moves.range(len(eval_data[j])):
            olab[0] = eval_data[j][k][1]
            start = max(bps[j][k] - overlap, 0)
            end = min(bps[j][k+1] + overlap-1, len(h1))
            g = attention(h1[start:end], s1['h1'], eval_data[j][k][2], train=False)
            s1 = decode_one_step(g, s1, y, train=False)
            maxlab,y = classify(s1['h1'])
            if maxlab == olab[0]:
                hit += 1
            count += 1
    acc = (float(hit)/count) * 100.0
    return hit, count, acc

############################################
print '----------------'
print 'Start training'
print '----------------'
sys.stdout.flush()
if args.log != '':
    flog.write('Start training\n')
    flog.flush()

# Setup optimizer
#optimizer = optimizers.SGD(lr=args.learn_rate)
optimizer = optimizers.AdaDelta()
optimizer.setup(model)

#random.shuffle(train_batchset)
cur_log_perp = xp.zeros(())
epoch = 0
start_at = time.time()
cur_at = start_at
accum_loss = chainer.Variable(xp.zeros((), dtype=np.float32))
overlap = args.overlap

model_saved = False
ilab = np.zeros((1,), dtype=np.int32)
olab = np.zeros((1,), dtype=np.int32)

# initial evaluation
valid_hit, valid_count, valid_acc = evaluate(valid_data, overlap, no_future=args.no_future)
print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
if args.log != '':
    flog.write('  initial valid accuracy = %g (%d/%d)\n' % (valid_acc,valid_hit,valid_count))
    flog.flush()
max_valid_acc = valid_acc

if args.test != '':
    test_hit, test_count, test_acc = evaluate(test_data, overlap, no_future=args.no_future)
    print ' test accuracy: %g (%d/%d)' % (test_acc, test_hit, test_count)
    if args.log != '':
        flog.write('  initial test accuracy = %g (%d/%d)\n' % (test_acc,test_hit,test_count))
        flog.flush()

# start training
n = 0
for i in six.moves.range(n_epochs):
    print '----------------'
    #print 'Epoch %d : learning rate = %g' % (i+1, optimizer.lr)
    print 'Epoch %d' % (i+1)
    if args.log != '':
        #flog.write('Epoch %d : learning rate = %g\n' % (i+1, optimizer.lr))
        flog.write('Epoch %d\n' % (i+1))
        flog.flush()
    random.shuffle(train_data)
    # get boundary info
    bps = []
    for j in six.moves.range(len(train_data)):
        m = 1
        bp = []
        for k in six.moves.range(len(train_data[j])):
            bp.append(m)
            m += (len(train_data[j][k][0]) + 1) # '+1' is added for <eos>
        bp.append(m)
        bps.append(bp)

    for j in six.moves.range(len(train_data)):
        # blstm encoding for j-th dialog
        h1 = blstm_encode(train_data[j], no_future=args.no_future)
        # loss accumulation
        s1 = make_initial_state(1)
        y = chainer.Variable(xp.zeros((1, len(olabelset)), dtype=np.float32),
                                   volatile=False)
        for k in six.moves.range(len(train_data[j])):
            olab[0] = train_data[j][k][1]
            start = max(bps[j][k] - overlap, 0)
            end = min(bps[j][k+1] + overlap-1, len(h1))
            g = attention(h1[start:end], s1['h1'], train_data[j][k][2])
            s1 = decode_one_step(g, s1, y)
            loss_i, y = cross_entropy(s1['h1'], olab)
            accum_loss += loss_i
            cur_log_perp += loss_i.data.reshape(())

            if (n + 1) % 1000 == 0:
                now = time.time()
                throuput = 1000. / (now - cur_at)
                perp = math.exp(cuda.to_cpu(cur_log_perp) / 1000)
                print(' iter {} training perplexity: {:.2f} ({:.2f} iters/sec)'.format(
                    n + 1, perp, throuput))
                cur_at = now
                cur_log_perp.fill(0)
            n += 1
        # Run truncated BPTT
        optimizer.zero_grads()
        accum_loss.backward()
        accum_loss.unchain_backward()  # truncate
        accum_loss = chainer.Variable(xp.zeros((), dtype=np.float32))

        optimizer.clip_grads(grad_clip)
        optimizer.update()

    print 'evaluate'
    now = time.time()
    valid_hit, valid_count, valid_acc = evaluate(valid_data, overlap, no_future=args.no_future)
    print ' valid accuracy: %g (%d/%d)' % (valid_acc, valid_hit, valid_count)
    if args.log != '':
        flog.write('  valid accuracy = %g (%d/%d)\n' % (valid_acc,valid_hit,valid_count))
        flog.flush()

    if args.test != '':
        test_hit, test_count, test_acc = evaluate(test_data, overlap, no_future=args.no_future)
        print ' test accuracy: %g (%d/%d)' % (test_acc, test_hit, test_count)
        if args.log != '':
            flog.write('  test accuracy = %g (%d/%d)\n' % (test_acc,test_hit,test_count))
            flog.flush()
    sys.stdout.flush()
    #cur_at += time.time() - now  # skip time of evaluation

    if max_valid_acc < valid_acc:
        print ' writing model params to', args.model
        if args.log != '':
            flog.write('  writing model params to %s\n' % args.model)
            flog.flush()
        model.to_cpu()
        fm = open(args.model, 'w')
        pickle.dump(ilabelset, fm);
        pickle.dump(olabelset, fm);
        pickle.dump(model, fm)
        fm.close()
        max_valid_acc = valid_acc
        model_saved = True
        if args.gpu >= 0:
            model.to_gpu()
#    else:
#        if model_saved==False:
#            print ' reloading model params from', args.initial_model
#            if args.log != '':
#                flog.write('  reloading model params from %s\n' % args.initial_model)
#                flog.flush()
#            fm = open(args.initial_model, 'r')
#        else:
#            print ' reloading model params from', args.model
#            if args.log != '':
#                flog.write('  reloading model params from %s\n' % args.model)
#                flog.flush()
#            fm = open(args.model, 'r')
#        ilabelset = pickle.load(fm)
#        olabelset = pickle.load(fm)
#        model = pickle.load(fm)
#        temp,n_units = model.parameters[2].shape
#        fm.close()
#        if args.gpu >= 0:
#            model.to_gpu()
#        #optimizer.lr *= args.learn_decay
#
#    optimizer.setup(model)
    sys.stdout.flush()
    cur_at += time.time() - now  # skip time of evaluation and file I/O

# Save model params
if model_saved == False:
    print ' writing model params to', args.model
    if args.log != '':
        flog.write('  writing model params to %s\n' % args.model)
        flog.flush()
    model.to_cpu()
    fm = open(args.model, 'w')
    pickle.dump(ilabelset,fm)
    pickle.dump(olabelset,fm)
    pickle.dump(model, fm)
    fm.close()

print('done')
if args.log != '':
    flog.write('done\n')
    flog.close()

