import json
import argparse
import nltk
from sklearn import svm
import numpy as np

parser = argparse.ArgumentParser()

parser.add_argument('--train', type=str, help='train file')
parser.add_argument('--valid', type=str, help='valid file')
parser.add_argument('--test', type=str, help='test file')

parser.add_argument('--wordvec', type=str, help='the path to word embedding')
parser.add_argument('--log', type=str, help='log file')

args = parser.parse_args()

# Load text data

def load_data(filename, train=True):
    
    global ilabelset
    global olabelset
    dataset = []
    
    print 'Loading', filename
    
    for s in open(filename).readlines():
        s = s.strip()
        data = s.split(":")
        olabel = data[0]	
        ilabels = data[1:]
        ilabels = ' '.join(ilabels)
        ilabels = ilabels.split("|")
	ilabel1 = ilabels[0].split()
    	ilabel2 = ilabels[1].split()
	#print ilabel1
	#print ilabel2	
	ilabels = ilabel1 + ilabel2
        seqdata = np.ndarray((len(ilabels),), dtype=np.int32)
  
        for i, ilabel in enumerate(ilabels):
            if ilabel in ilabelset:
                seqdata[i] = ilabelset[ilabel]
            else:
                if train==True:
                    seqdata[i] = len(ilabelset)
                    ilabelset[ilabel] = len(ilabelset)
                else:
                    seqdata[i] = unk

        if olabel in olabelset:
	   #print "get it"
           olabel_id = olabelset[olabel]
        else:
           if train==True:
              #print "add"	
              olabel_id = len(olabelset)
              olabelset[olabel] = len(olabelset)
           else:
              olabel_id = unk

        dataset.append((seqdata, olabel_id, ilabels, olabel, ilabel1, ilabel2))
    return dataset


def load_wordvec(filename):
    
    global wordvec
        
    for s in open(filename).readlines():
    	s = s.split()
	word = s[0]
	vec = s[2:-1]
	lst = [float(x[1:-2]) for x in vec]
	wordvec[word] = lst

    print 'word vec: ', len(wordvec)	


def train_WordVec_SVM(dataset, fp):
	
    clf = svm.SVC()
    X = []
    Y = []
    idx = 0
			
    for data in dataset:	
	if idx%1000 == 0:
	   print idx
	
	idx += 1
	label = data[1]	
	ilabel1 = data[4]
        ilabel2 = data[5]	
	
	x = [0]*(len(wordvec["the"]))
	valid = 0
	for word in ilabel1:
	     if word in wordvec.keys():  	
	     	x = [a + b for a, b in zip(x, wordvec[word])]
		valid += 1
	if valid >= 1:	
	   x1 = [m/valid for m in x] 
	
	x = [0]*(len(wordvec["the"]))
        valid = 0
        for word in ilabel2:
             if word in wordvec.keys():
                x = [a + b for a, b in zip(x, wordvec[word])]
                valid += 1

        if valid >= 1:
           x2 = [m/valid for m in x]

	x_con = x1 + x2 # concatenate the feature
	X.append(x_con)	
	Y.append(label)		
	
    clf.fit(X, Y)
    print "train accuracy: ", clf.score(X, Y)
    log = "train accuracy:" + str(clf.score(X, Y)) + "\n"	
    fp.write(log)	
    return clf


def test_WordVec_SVM(dataset, clf, fp):
	
    X = []
    Y = []
		
    for data in dataset:	
	label = data[1]	
	ilabel1 = data[4]
        ilabel2 = data[5]	
	
	x = [0]*(len(wordvec["the"]))
	valid = 0
	for word in ilabel1:
	     if word in wordvec.keys():  	
	     	x = [a + b for a, b in zip(x, wordvec[word])]
		valid += 1

	if valid >= 1:	
	   x1 = [m/valid for m in x]
	
	x = [0]*(len(wordvec["the"]))
        valid = 0
        for word in ilabel2:
             if word in wordvec.keys():
                x = [a + b for a, b in zip(x, wordvec[word])]
                valid += 1

	if valid >= 1:
           x2 = [m/valid for m in x]

	x_con = x1 + x2 # concatenate the feature 
	X.append(x_con)	
	Y.append(label)				
	
    print "the test acuracy: ", clf.score(X, Y)

    log = "test accuracy:" + str(clf.score(X, Y)) + "\n"       
    fp.write(log)


ilabelset = {}
olabelset = {}
wordvec = {}

unk = -1
print "load training data"
traindata = load_data(args.train, train=True)
print "load dev data"
validdata = load_data(args.valid, train=False)
print "load test data"
testdata = load_data(args.test, train=False)
print 'dict: ', len(ilabelset)
print 'label: ', len(olabelset)
print "load word vector"
load_wordvec(args.wordvec)

print 'now doing word embedding experiment'
fp = open(args.log, "wt+")
svm_wordvec = train_WordVec_SVM(traindata, fp)
test_WordVec_SVM(testdata, svm_wordvec, fp)
fp.close()

