import json
import argparse
import nltk
from sklearn import svm
import numpy as np

parser = argparse.ArgumentParser()

parser.add_argument('--train', type=str, help='train file')
parser.add_argument('--valid', type=str, help='valid file')
parser.add_argument('--test', type=str, help='test file')

parser.add_argument('--wordvec', type=str, help='the path to original word embedding')
parser.add_argument('--wordvec_result', '-wr', type=str, help='the path to save the word embedding')

args = parser.parse_args()

# Load text data

def load_data(filename, train=True):
    
    global ilabelset
    global olabelset
    dataset = []
    
    print 'Loading', filename
    
    for s in open(filename).readlines():
        s = s.strip()
        data = s.split(":")
        olabel = data[0]
	#print olabel
        ilabels = data[1:]
        ilabels = ' '.join(ilabels)
        ilabels = ilabels.split()
        seqdata = np.ndarray((len(ilabels),), dtype=np.int32)
        #print len(ilabels)
        for i, ilabel in enumerate(ilabels):
            if ilabel in ilabelset:
                seqdata[i] = ilabelset[ilabel]
            else:
                if train==True:
                    seqdata[i] = len(ilabelset)
                    ilabelset[ilabel] = len(ilabelset)
                else:
                    seqdata[i] = unk

        if olabel in olabelset:
	   #print "get it"
           olabel_id = olabelset[olabel]
        else:
           if train==True:
              #print "add"	
              olabel_id = len(olabelset)
              olabelset[olabel] = len(olabelset)
           else:
              olabel_id = unk

        dataset.append((seqdata, olabel_id, ilabels, olabel))

    return dataset

def load_wordvec(filename):
    
    global wordvec
    idx = 0    
    for s in open(filename).readlines():

	if idx%1000 == 0:
	   print idx
	idx += 1
    	s = s.split()
	word = s[0]
	vec = s[:-1]
	if word in ilabelset.keys():
	   wordvec[word] = vec

    print 'effective wordvec: ', len(wordvec)	

def save_wordvec(filename):
   
    fp = open(filename, "wt+")     
    for word in wordvec.keys():
	line = word + " " + str(wordvec[word]).strip('[]')
	fp.write(line + "\n")

    fp.close()


ilabelset = {}
olabelset = {}
wordvec = {}

unk = -1
print "load training data"
traindata = load_data(args.train, train=True)
print "load dev data"
validdata = load_data(args.valid, train=False)
print "load test data"
testdata = load_data(args.test, train=False)
print 'dict: ', len(ilabelset)
print 'label: ', len(olabelset)
print "load word vector"
load_wordvec(args.wordvec)


print 'write the word embedding'
save_wordvec(args.wordvec_result)	
