python data_reader.py --inputfile ../data/snli_1.0_train.jsonl --outputfile ../data/snli_1.0_train_plain.txt
python data_reader.py --inputfile ../data/snli_1.0_test.jsonl --outputfile ../data/snli_1.0_test_plain.txt
python data_reader.py --inputfile ../data/snli_1.0_dev.jsonl --outputfile ../data/snli_1.0_dev_plain.txt
