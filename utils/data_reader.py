import json
import argparse
import nltk

parser = argparse.ArgumentParser()

parser.add_argument('--inputfile', type=str, help='inputfile')
parser.add_argument('--outputfile', type=str, help='outputfile')

args = parser.parse_args()


def nltk_token_sentence_to_tokens(text):
    tokens = nltk.word_tokenize(text)
    return tokens

def read_data(inputfilename, outputfilename):
    

    fp = open(outputfilename, 'wt+')
    with open(inputfilename) as data_file:  
         count = 0
         for line in data_file:
             data = json.loads(line)
  	     sen1 = data['sentence1'].lower()	
             sen1 = nltk_token_sentence_to_tokens(sen1) 
             sen2 = data['sentence2'].lower()
             sen2 = nltk_token_sentence_to_tokens(sen2)
             label = data['gold_label']
             #print label
             if label != '-': # tied label
                line = label + ':' + ' '.join(sen1) + ' | ' + ' '.join(sen2) + '\n'
                fp.write(line)
		count += 1
 
    print 'instance, ', count
    fp.close()

read_data(args.inputfile, args.outputfile)  
